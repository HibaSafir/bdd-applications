<?php
require 'vendor/autoload.php';
require "vendor/fzaninotto/faker/src/autoload.php";
use \bdd\modele\Character;
use \bdd\modele\Company;
use \bdd\modele\Game;
use \bdd\modele\Gamerating;
use \bdd\modele\Genre;
use \bdd\modele\Platform;
use \bdd\modele\Ratingboard;
use \bdd\modele\Theme;
use \bdd\modele\User;
use \bdd\modele\Comment;
use \bdd\controleur\ControleurJeu;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection( [
 'driver' => 'mysql',
 'host' => 'webetu.iutnc.univ-lorraine.fr',
 'database' => 'safir1u',
 'username' => 'safir1u',
 'password' => 'azerty',
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();
//
// //q1
// // $v = Game::where('name', 'like', '%Mario%')->get();
// // foreach ($v as $key => $value) {
// //   echo($value->name);
// // }
// //
// // $v2 = Company::where('location_country', '=', 'Japan')->get();
// // foreach ($v2 as $key => $value) {
// //   echo($value->name);
// // }
// //
// // $v3 = Platform::where('install_base', '>=', '10000000')->get();
// // foreach ($v3 as $key => $value) {
// //   echo($value->name);
// // }
//
// // $v4 = Game::where('id', '>=', '21173')->where('id', '<=', '21615')->get();
// // foreach ($v4 as $key => $value) {
// //   echo($value->name);
// // }
//
// //$v4 = Game::get()->paginate(500);
// // foreach ($v4 as $key => $value) {
// //   echo($value->name . " " . $value->deck);
// // }
//
//
// //Seance 2
//
// //q1
// /*
// $r = Game::find(12342);
// $listperso=$r->jeu_perso;
//  foreach ($listperso as $key => $value) {
// 	echo($value->name . " " . $value->deck);
// }
// */
// //q2
// /*
// $r1 = Game::where('name', 'like', 'Mario%')->get();
// foreach ($r1 as $cle => $valeur) {
// 	echo "********JEU***** " . $valeur->name . "\n";
// $listperso2=$valeur->jeu_perso;
//  foreach ($listperso2 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------";
// }
// echo "\n";
// }*/
//  //q3
//  /*
// $r3 = Company::where('name', 'like', '%Sony%')->get();
// foreach ($r3 as $cle=>$valeur) {
// 	echo "********COMPANY******** " . $valeur->name . "\n";
// $r4 = $valeur->developpement;
//  foreach ($r4 as $key => $value) {
// 	echo "---" . ($value->name) . " --";
//  }
//  echo "\n";
// }
// */
// //q4
// /*
// $r5 = Game::where('name', 'like', '%Mario%')->get();
// foreach ($r5 as $cle => $valeur) {
// 	echo "********JEU***** " . $valeur->name . "\n";
// $listperso2=$valeur->jeu_classement;
//  foreach ($listperso2 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------";
// }
// echo "\n";
// }
// */
// //q5
// /*
// $r5 = Game::where('name', 'like', 'Mario%')->has('jeu_perso', '>', 3)->get();
// foreach ($r5 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------" . "\n";
// }
//
// */
// //q6
// /*
// $r6 = Game::where('name', 'like', 'Mario%')->whereHas('jeu_classement', function($q) {
// 	$q->where('name', 'like', '%3+%');
// })->get();
// foreach ($r6 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------" . "\n";
// }
// */
// //q7
// /*
// $r7 = Game::where('name', 'like', 'Mario%')
// ->whereHas('jeu_classement', function($q) {
// 	$q->where('name', 'like', '%3+%');})
// ->whereHas('jeu_developpement', function($q) {
// $q->where('name', 'like', '%Inc.%');})->get();
// foreach ($r7 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------" . "\n";
// }
// */
// //q8
// /*
// $time_debut = microtime(true);
// $r8 = Game::where('name', 'like', 'Mario%')
// ->whereHas('jeu_classement', function($q) {
// 	$q->where('name', 'like', '%3+%');})
// ->whereHas('jeu_developpement', function($q) {
// $q->where('name', 'like', '%Inc.%');})
// ->whereHas('jeu_classement.rating_board', function($q) {
// 	$q->where('name', 'like', 'CERO');})->get();
// foreach ($r8 as $key => $value) {
// 	echo "---------" . ($value->name) . " ----------" . "\n";
// }
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo $temps;
// */
//
// //q9
// /**$nouveaugenre = new Genre();
// $nouveaugenre->save();
// $jeu12=Game::find(12);
// $jeu56=Game::find(56);
// $jeu345=Game::find(345);
// $nouveaugenre->genre_jeu()->attach([12,56,345]);
// */
// //Partie 1
//
// /**
// $time_debut = microtime(true);
// $jeux = Game::get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo $temps;
// */
//
//
// $time_debut = microtime(true);
// $jeux = Game::where("name", "like", "%Mario%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "Jeux contenant Mario ".$temps;
//
// /**
// $time_debut = microtime(true);
// $jeux = Game::where('name', 'like', 'Mario%')->get();
// foreach ($jeux as $valeur) {
//   $listperso2=$valeur->jeu_perso;
// }
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\nListe perso des jeux contenant Mario ".$temps;
// */
//
// $time_debut = microtime(true);
// $jeux = Game::where('name', 'like', 'Mario%')->whereHas('jeu_classement', function($q) {
// 	$q->where('name', 'like', '%3+%');
// })->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\nJeux contenant Mario et rating 3+ ".$temps;
// /**
// *La premiere requete ne s'exécute pas car elle prend beaucoup de mémoires vu le nombre de lignes qu'il y a dans la table Game
// *les autres requêtes s'exécutent plus rapidement car il suffit de parcourir les lignes récupérées grâce aux conditions where faites
// */
// // Partie 2
// /**
// $time_debut = microtime(true);
// $jeux = Game::where("name", "like", "%Ninja%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "Jeux contenant Ninja ".$temps;
// $time_debut = microtime(true);
// $jeux = Game::where("name", "like", "%Sonic%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\nJeux contenant Sonic ".$temps;
// $time_debut = microtime(true);
// $jeux = Game::where("name", "like", "%Mario%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\nJeux contenant Mario ".$temps;
//
// //index créé dans phpmyadmin sur name de la table game;
// /**
// *L'index permet de trier les données afin de pouvoir les trouver plus rapidement
// *L'index placé sur la colonne name, on recherche dans la base les données sur l'index et s'il trouve ce qu'il cherche, il saura plus rapidement
// *où se trouve les données recherchées.
// *Il n'y a pas un grand changement sur le temps d'exécution même après avoir placé l'index
// */
// // Par rapport aux compagnies, après avoir placé un index sur la colonne location_country
// $time_debut = microtime(true);
// $comp = Company::where("location_country", "like", "%Japan%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\n compagnies du Japon ".$temps;
//
// $time_debut = microtime(true);
// $comp = Company::where("location_country", "like", "%USA%")->get();
// $time_fin = microtime(true);
// $temps = $time_fin-$time_debut;
// echo "\n compagnies du USA ".$temps;
//
// Il n'y a pas vraiment de gain de temps même après avoir ajouté l'index parce qu'il n'y a pas beaucoup de valeurs différentes
// //la recherche s'effectue dans un premier temps sur l'index ensuite après l'avoir trouvé sur les données qui sont trouvées
// //rapidement ensuite






/*
//1
$jeux = Game::where("name", "like", "%Mario%")->get();
//2
$jeuxperso = Game::find(12342)->jeu_perso()->get();
//3
$jeux3 = Game::where("name", "like", "%Mario%")->get();
foreach ($jeux3 as $cle=>$valeur) {
  $valeur->character;
}

//4
$jeuxMario = Game::where('name', 'like', '%Mario%')->get();
foreach ($jeuxMario as $cle=>$valeur) {
  $valeur->jeu_perso;
}
//5
$jeuxComp = Company::where('name', 'like', '%Sony%')->get();
foreach ($jeuxComp as $cle=>$valeur) {
  $valeur->developpement;
}


*/










// $jeux3 = Game::where("name", "like", "%Mario%")->with("character")->get();
// foreach ($jeux3 as $valeur) {
//   $valeur->character;
// }
//2 requetes, utilisation de 'in' pour recuperer les personnages
// $jeuxComp = Company::where('name', 'like', '%Sony%')->with('developpement')->get();
// foreach ($jeuxComp as $cle=>$valeur) {
//   $valeur->developpement;
// }
//
// $tab = DB::getQueryLog();
// foreach($tab as $cle=>$valeur) {
// $lcWhatYouWant = $tab[$cle]['query'];
// echo "\n---------------------------";
// echo "\n" . $lcWhatYouWant;
// echo "\n---------------------------";
// }

//Partie 1
$faker = Faker\Factory::create();
// User::createUser($faker->firstName,$faker->lastName,$faker->address,$faker->date('d-m-Y','now'),$faker->e164PhoneNumber,$faker->email);
// User::createUser($faker->firstName,$faker->lastName,$faker->address,$faker->date('d-m-Y','now'),$faker->e164PhoneNumber,$faker->email);
//
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(), User::find(1),Game::find(12342));
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(), User::find(1),Game::find(12342));
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(), User::find(2),Game::find(12342));
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(), User::find(2),Game::find(12342));
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(), User::find(2),Game::find(12342));


//Partie 2
// for ($i=0; $i < 25000; $i++) {
//   User::createUser($faker->firstName,$faker->lastName,$faker->address,$faker->date('d-m-Y','now'),$faker->e164PhoneNumber,$faker->email);
// }


// $jeu=Game::find(round(rand()*25046)+1);
// Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(),$u, $jeu);
// for ($i=0; $i < 250000; $i++) {
//   $u=User::find(rand(1,25048));
//   $jeu=Game::find(rand(1, 47948));
//
//   Comment::createComment($faker->text(50),$faker->text(99),$faker->date(), $faker->date(),$u, $jeu);
// }
//q1
// $us = User::find(1);
// $comment=$us->comment()->orderby('created_at', 'DESC')->get();
// foreach ($comment as $cle=>$valeur) {
//   echo $valeur->content . " " . $valeur->created_at . "\n";
// }
//q2
// $us1 = User::has('comment', '>', 5)->get();
// foreach ($us1 as $cle=>$valeur) {
//   echo $valeur->firstName . " " . $valeur->lastName . "\n";
// }



$app = new \Slim\Slim();


$app->get('/api/games/:id', function($id) {
  ControleurJeu::afficherJeu($id);
})->name('afficherJeu');

$app->get('/api/games', function(){
  ControleurJeu::afficherListe();
})->name('listeJeu');

$app->get('/api/games/:id/comments', function($id){
  ControleurJeu::commentairesJeu($id);
})->name('commentairesJeu');

$app->post('/api/games/:id/comments', function($id){
  ControleurJeu::ajouterCommentaire($id);
});

$app->get('/api/character/:id', function($id){

})->name('character');

$app->get('/api/comment/:id', function($id){

})->name('comment');

$app->get('/api/games/:id/characters', function($id){
  ControleurJeu::personnagesJeu($id);
})->name('personnagesJeu');

$app->run();

?>
