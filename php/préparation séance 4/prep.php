<?php
//on uilise composer require fzaninotto/faker pour installer faker

require "../vendor/fzaninotto/faker/src/autoload.php";

$faker = Faker\Factory::create();

echo $faker->address;

echo ($faker->dateTime($max = 'now', $timezone = null))->format('Y/m/d (h:i)');
