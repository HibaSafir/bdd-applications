<?php
require 'vendor/autoload.php';
require "vendor/fzaninotto/faker/src/autoload.php";
use \bdd\modele\Character;
use \bdd\modele\Company;
use \bdd\modele\Game;
use \bdd\modele\Gamerating;
use \bdd\modele\Genre;
use \bdd\modele\Platform;
use \bdd\modele\Ratingboard;
use \bdd\modele\Theme;
use \bdd\modele\User;
use \bdd\modele\Comment;

public class VueJeu{

  public static void affichageJeu($jeu){
    $app = \Slim\Slim::getInstance();
    echo $jeu;
  }
}
