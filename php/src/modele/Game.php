<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Game extends Model {
  protected $table = 'game';
  protected $primaryKey='id';
  public $timestamps = false;

  public function character() {
    return $this->belongsTo('\bdd\modele\Character', 'game_id');
  }

  public function comments(){
    return $this->hasMany('\bdd\modele\Comment', 'game_id');
  }


  public function jeu_developpement() {
    return $this->belongsToMany('\bdd\modele\Company', 'game_developers', 'game_id', 'comp_id');
  }
  public function jeu_publication() {
    return $this->belongsToMany('\bdd\modele\Company', 'game_publishers', 'game_id', 'comp_id');
  }

  public function jeu_platform() {
    return $this->belongsToMany('\bdd\modele\Platform', 'game2platform', 'game_id', 'platform_id');
  }

  public function jeu_genre() {
    return $this->belongsToMany('\bdd\modele\Genre', 'game2genre', 'game_id', 'genre_id');
  }

  public function jeu_theme() {
    return $this->belongsToMany('\bdd\modele\Theme', 'game2theme', 'game_id', 'theme_id');
  }

  public function jeu_perso() {
    return $this->belongsToMany('\bdd\modele\Character', 'game2character', 'game_id', 'character_id');
  }

  public function jeu_classement() {
    return $this->belongsToMany('\bdd\modele\Gamerating', 'game2rating', 'game_id', 'rating_id');
  }
}
