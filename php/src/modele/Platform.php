<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Platform extends Model {
  protected $table = 'platform';
  protected $primaryKey='id';
  public $timestamps = false;

  public function company(){
    return $this->hasMany('\bdd\modele\Company', 'comp_id');
  }

  public function platform_jeu() {
    return $this->belongsToMany('\bdd\modele\Game', 'game2platform', 'platform_id', 'game_id');
  }

}
