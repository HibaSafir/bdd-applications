<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
use \bdd\modele\Game;
class Comment extends Model {
  protected $table = 'comment';
  protected $primaryKey='id';
  public $timestamps = true;

  public function user(){
    return $this->belongsTo('\bdd\modele\User', 'user_id');
  }

  public function game(){
    return $this->belongsTo('\bdd\modele\Game', 'game_id');
  }

  public static function createComment($t, $con, $creat, $upd, $u, $game){
    $c = new Comment();
    $c->title = $t;
    $c->content = $con;
    $c->created_at = $creat;
    $c->updated_at = $upd;
    $u->comment()->save($c);
    $game->comments()->save($c);
    return $c->id;
  }
}
