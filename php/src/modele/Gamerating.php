<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Gamerating extends Model {
  protected $table = 'game_rating';
  protected $primaryKey='id';
  public $timestamps = false;

public function rating_board() {
	return $this->belongsTo('\bdd\modele\Ratingboard', 'rating_board_id');
}

public function classement_jeu() {
  return $this->belongsToMany('\bdd\modele\Game', 'game2rating', 'rating_id', 'game_id');
}
}
