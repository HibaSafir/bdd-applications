<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Character extends Model {
  protected $table = 'character';
  protected $primaryKey='id';
  public $timestamps = false;

  public function game(){
    return $this->hasMany('\bdd\modele\Game', 'game_id');
  }

  public function ami() {
    return $this->belongsToMany('\bdd\modele\Character', 'Character', 'char1_id', 'char2_id');
  }

  public function ennemi() {
    return $this->belongsToMany('\bdd\modele\Character', 'Character', 'char1_id', 'char2_id');
  }

  public function perso_jeu() {
    return $this->belongsToMany('\bdd\modele\Game', 'game2character', 'character_id', 'game_id');
  }

}
