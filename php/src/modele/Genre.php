<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Genre extends Model {
  protected $table = 'genre';
  protected $primaryKey='id';
  public $timestamps = false;

  public function genre_jeu() {
    return $this->belongsToMany('\bdd\modele\Game', 'game2genre', 'genre_id', 'game_id');
  }
}
