<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Company extends Model {
  protected $table = 'company';
  protected $primaryKey='id';
  public $timestamps = false;

  public function platform() {
    return $this->belongsTo('\bdd\modele\Platform', 'comp_id');
  }

  public function publication() {
    return $this->belongsToMany('\bdd\modele\Game', 'game_publishers', 'comp_id', 'game_id');
  }
  public function developpement() {
    return $this->belongsToMany('\bdd\modele\Game', 'game_developers', 'comp_id', 'game_id');
  }



}
