<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;

class Ratingboard extends Model {
  protected $table = 'rating_board';
  protected $primaryKey='id';
  public $timestamps = false;

  public function game_rating() {
    return $this->hasMany('\bdd\modele\Gamerating', 'rating_board_id');
  }
}
