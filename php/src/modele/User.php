<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class User extends Model {
  protected $table = 'user';
  protected $primaryKey='id';
  public $timestamps = false;

  public static function createUser($first, $last, $adr, $birth, $phone, $em){
    $u = new User();
    $u->firstName = $first;
    $u->lastName = $last;
    $u->address = $adr;
    $u->birthDate = $birth;
    $u->phoneNumber = $phone;
    $u->email = $em;
    $u->save();
  }

  public function comment(){
    return $this->hasMany('\bdd\modele\Comment', 'user_id');
  }

}
