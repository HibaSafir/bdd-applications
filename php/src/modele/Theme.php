<?php
namespace bdd\modele;
use Illuminate\Database\Eloquent\Model;
class Theme extends Model {
  protected $table = 'theme';
  protected $primaryKey='id';
  public $timestamps = false;

  public function theme_jeu() {
    return $this->belongsToMany('\bdd\modele\Game', 'game2theme', 'theme_id', 'game_id');
  }
}
