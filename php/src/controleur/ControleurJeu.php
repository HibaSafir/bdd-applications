<?php
namespace bdd\controleur;
require 'vendor/autoload.php';
require "vendor/fzaninotto/faker/src/autoload.php";
use \bdd\modele\Character;
use \bdd\modele\Company;
use \bdd\modele\Game;
use \bdd\modele\Gamerating;
use \bdd\modele\Genre;
use \bdd\modele\Platform;
use \bdd\modele\Ratingboard;
use \bdd\modele\Theme;
use \bdd\modele\User;
use \bdd\modele\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class ControleurJeu{

  public static function afficherJeu($id){
    $app=\Slim\Slim::getInstance();
    $app->response->headers->set('Content-type', 'application/json');
    $uri = $app->request->getResourceUri();
    try{
      $jeu = Game::findOrFail($id);
      $objet = array("id"=>$jeu->id, "name"=>$jeu->name, "alias"=>$jeu->alias, "deck"=>$jeu->deck, "description"=>$jeu->description, "original_release_date"=>$jeu->original_release_date);
      echo json_encode(array("game"=>$objet, "links"=>["comments"=>["href"=>$uri."/comments"], "characters"=>["href"=>$uri."/characters"]]));
    } catch(ModelNotFoundException $e){
      $app->response->setStatus(404);
      echo json_encode(["msg"=>"game $id not found"]);
    }
  }

  public static function afficherListe(){
    $app=\Slim\Slim::getInstance();
    $app->response->headers->set('Content-type', 'application/json');
    $page = $app->request->get("page");
    $uri =  $app->request->getResourceUri();
    try{
      $tab = array();
      $jeu = Game::where("id", ">", $page*200)->where("id", "<", $page*200+200)->get();
      foreach($jeu as $v){
        $tab[] = ["game"=>["id"=>$v->id, "name"=>$v->name, "alias"=>$v->alias, "deck"=>$v->deck], "links"=>["self"=>["href"=>$uri."/".$v->id]]];
      }
      echo json_encode(array("games"=>$tab, "links"=>["prev"=>["href"=>$uri.'?page='.($page-1)], "next"=>["href"=>$uri.'?page='.($page+1)]]));
    } catch(ModelNotFoundException $e){
      $app->response->setStatus(404);
      echo json_encode(["msg"=>"Erreur"]);
    }
  }

  public static function commentairesJeu($id){
    $app=\Slim\Slim::getInstance();
    $app->response->headers->set('Content-type', 'application/json');
    $page = $app->request->get("page");
    $uri =  $app->request->getResourceUri();
    try{
      $tab = array();
      $jeu = Game::findOrFail($id);
      $commentaires = $jeu->comments;
      foreach($commentaires as $v){
        $user = $v->user;
        $tab[] = ["id"=>$v->id, "title"=>$v->title, "content"=>$v->content, "created_at"=>$v->created_at, "userName"=>$user->lastName];
      }
      echo json_encode(array("game"=>$tab));
    } catch(ModelNotFoundException $e){
      $app->response->setStatus(404);
      echo json_encode(["msg"=>"game $id not found"]);
    }
  }

  public static function personnagesJeu($id){
    $app=\Slim\Slim::getInstance();
    $app->response->headers->set('Content-type', 'application/json');
    $page = $app->request->get("page");
    try{
      $tab = array();
      $jeu = Game::findOrFail($id);
      $c = $jeu->jeu_perso;
      foreach($c as $v){
        $user = $v->user;
        $tab[] = ["character"=>["id"=>$v->id, "name"=>$v->name], "links"=>["self"=>["href"=>"/api/characters/".$v->id]]];
      }
      echo json_encode(array("characters"=>$tab));
    } catch(ModelNotFoundException $e){
      $app->response->setStatus(404);
      echo json_encode(["msg"=>"game $id not found"]);
    }
  }

  public static function ajouterCommentaire($id){
    $app=\Slim\Slim::getInstance();
    $idComm;
    //  $app->response->headers->set('Content-type', 'application/json');
    try{
      $body = $app->request->getBody();
      $comment = json_decode($body, true);
      $titre = $comment['title'];
      $email = $comment['email'];
      $contenu = $comment['content'];
      $user = User::where("email", "=", $email)->first();
      $game = Game::findOrFail($id);
      if(!is_string($titre)||! is_string($email) || !is_string($contenu)){
        throw new Exception("Mauvais type");
      }
      if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
          throw new Exception("Erreur filtre email non valide");
      }
      $idComm = Comment::createComment($titre, $contenu, date('Y-m-d'), date('Y-m-d'), $user, $game);
    } catch(ModelNotFoundException $e){
      $app->response->setStatus(404);
      echo json_encode(["msg"=>"game $id not found"]);
      return null;
    }catch(Exception $e){
      $app->response->setStatus(400);
      echo json_encode(["msg"=>$e->getMessage()]);
      return null;
    }
    $app->response->setStatus(201);
    $res = explode('index.php', $app->urlFor("comment"));
    $uri = str_replace(":id", $idComm, $res[1]);
    $app->response->headers->set('Location', $uri);
  }
}

?>
