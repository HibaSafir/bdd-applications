<?php
namespace bdd\php;
use Illuminate\Database\Eloquent\Model;
class Character extends Model {
  protected $table = 'character';
  protected $primaryKey='id';
  public $timestamps = false;

  public function game(){
    return $this->hasMany('\bdd\php\Game', 'id');
  }

}
